import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import axios from 'axios'
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/styles';
import Container from '@material-ui/core/Container';

const styles = theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = { username: '', password: '', error: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    const { name, value } = event.target
    this.setState({ [name]: value });
  }
  handleSubmit(event) {
    event.preventDefault();
    const { username, password } = this.state
    axios.post('http://212.47.243.205:8000/login/', {
      username, password
    })
      .then(response => {
        if (response && response.status === 200) {
          const { token } = response.data
          localStorage.setItem("auth", JSON.stringify({ username, auth: true, token }));
          this.setState({ error: ''})
          this.props.syncAuth()
        } else {
          this.setState({ error: 'Something went wrong'})
        }
      })
      .catch(error => {
        console.log(error);
        this.setState({ error: 'Invalid user credentials' });
      });
  }
  render() {
    const { classes } = this.props
    const { username, password, error } = this.state
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
        </Typography>
          {error && <p style={{ color: 'red' }}>{error}</p>}
          <form onSubmit={this.handleSubmit} className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="username"
              label="Username"
              name="username"
              value={username}
              onChange={this.handleChange}
              autoComplete="username"
              autoFocus
              required
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              name="password"
              label="Password"
              type="password"
              value={password}
              onChange={this.handleChange}
              id="password"
              autoComplete="current-password"
              required
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
          </Button>
          </form>
        </div>
      </Container>
    )
  };
}

export default withStyles(styles)(SignIn)
