import React from 'react';
import axios from 'axios'
import { withStyles } from '@material-ui/styles';
import isEmpty from 'lodash/isEmpty'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import StyledTableRow from './StyledTableRow'
import StyledTableCell from './StyledTableCell'
import { stableSort, getSorting, stringEllipsis, convertDate } from './util'
import EnhancedTableHead from './EnhancedTableHead'
import EnhancedTableToolbar from './EnhancedTableToolbar'
import MultiSelection from '../../common/MultiSelection'
import headRows from './headrows.json'

// const DepartmentSuggestions = ['Paris', 'Landon']
//     .map(suggestion => ({
//         value: suggestion,
//         label: suggestion,
//     }));

// const assetSuggestions = ['Apartment', 'Parking', 'House']
//     .map(suggestion => ({
//         value: suggestion,
//         label: suggestion,
//     }));

// const salesSuggestions = ['Ventes immobilières', 'Other']
//     .map(suggestion => ({
//         value: suggestion,
//         label: suggestion,
//     }));

// const regionSuggestions = ['Ile-de-France', 'Other']
//     .map(suggestion => ({
//         value: suggestion,
//         label: suggestion,
//     }));

const EnhancedTableStyles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    gridContainer: {
        flexGrow: 1,
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    table: {
        minWidth: 750,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
});


class EnhancedTable extends React.Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = {
            open: false,
            order: 'asc', data: {},
            orderBy: 'calories',
            selected: [],
            page: 0,
            dense: false,
            rowsPerPage: 10,
            department: [],
            departmentSuggestions: [],
            assetType: [],
            assetSuggestions: [],
            salesType: [],
            salesSuggestions: [],
            region: [],
            regionSuggestions: [],
            website: [],
            websiteSuggestions: [],
            showFilter: false
        };
    }
    componentDidMount() {
        this.fetchData()
        const callback = () => {
            this.setState({ showFilter: true })
        }
        this.fetchFilterOption(callback)
    }
    fetchFilterOption = (callback) => {

        /*
        axios.get('http://51.15.77.48/choices/')
            .then(response => {
                const data = response.data
                const departmentSuggestions = []
                if (data.department) {
                    data.department.forEach((item) => {
                        if (item[0] === null) {
                            departmentSuggestions.push({
                                label: 'None',
                                value: ''
                            })
                        } else {
                            departmentSuggestions.push({
                                label: item[0],
                                value: item[1]
                            })
                        }
                    })
                }
                const assetSuggestions = []
                if (data.asset_type) {
                    data.asset_type.forEach((item) => {
                        if (item[0] === null) {
                            assetSuggestions.push({
                                label: 'None',
                                value: ''
                            })
                        } else {
                            assetSuggestions.push({
                                label: item[0],
                                value: item[1]
                            })
                        }
                    })
                }
                const salesSuggestions = []
                if (data.sales_type) {
                    data.sales_type.forEach((item) => {
                        if (item[0] === null) {
                            salesSuggestions.push({
                                label: 'None',
                                value: ''
                            })
                        } else {
                            salesSuggestions.push({
                                label: item[0],
                                value: item[1]
                            })
                        }
                    })
                }
                const regionSuggestions = []
                if (data.region) {
                    data.region.forEach((item) => {
                        if (item[0] === null) {
                            regionSuggestions.push({
                                label: 'None',
                                value: ''
                            })
                        } else {
                            regionSuggestions.push({
                                label: item[0],
                                value: item[1]
                            })
                        }
                    })
                }
                const websiteSuggestions = []
                if (data.website_name) {
                    data.website_name.forEach((item) => {
                        if (item[0] === null) {
                            websiteSuggestions.push({
                                label: 'None',
                                value: ''
                            })
                        } else {
                            websiteSuggestions.push({
                                label: item[0],
                                value: item[1]
                            })
                        }
                    })
                }
                this.setState({
                    departmentSuggestions,
                    assetSuggestions,
                    salesSuggestions,
                    regionSuggestions,
                    websiteSuggestions
                },
                    () => {
                        callback()
                    })
            })
            .catch(error => {
                alert('Something went wrong')
            })
        */
    }
    fetchData = () => {
        let { page, rowsPerPage, department, assetType, salesType, region, website } = this.state
        department = department.map(item => item.value)
        assetType = assetType.map(item => item.value)
        salesType = salesType.map(item => item.value)
        region = region.map(item => item.value)
        website = website.map(item => item.value)
        const params = new URLSearchParams();
        params.append("format", 'json');
        department.forEach(item => {
            params.append("department", item);
        })
        assetType.forEach(item => {
            params.append("asset_type", item);
        })
        salesType.forEach(item => {
            params.append("sales_type", item);
        })
        region.forEach(item => {
            params.append("region", item);
        })
        website.forEach(item => {
            params.append("website_name", item);
        })
        axios.get('http://212.47.243.205:8000/annonces/realestate/all?format=json', {
            params
        })
            .then(response => {
                this.setState({ data: response.data })
            })
            .catch(error => {
                alert('Something went wrong')
            })
    }

    handleRequestSort = (event, property) => {
        const { orderBy, order } = this.state
        const isDesc = orderBy === property && order === 'desc';
        this.setState({ order: isDesc ? 'asc' : 'desc', orderBy: property })
    }
    handleChange = event => {
        const { name, value } = event.target
        this.setState({ [name]: value })
    }
    handleChangePage = (event, page) => {
        this.setState({ page })
    }

    handleChangeRowsPerPage = (event) => {
        this.setState({ rowsPerPage: +event.target.value })
    }

    handleSelection = (name, val) => {

        this.setState({ [name]: val })
    }

    handleChangeDense = (event) => {
        this.setState({ dense: event.target.checked })
    }
    handleClickOpen = () => {
        this.setState({ open: true })
    }
    handleClose = () => {
        this.setState({
            open: false,
            department: [],
            assetType: [],
            salesType: [],
            region: [],
            website: [],
        })
    }
    handleClear = () => {
        this.setState({
            open: false,
            department: [],
            assetType: [],
            salesType: [],
            region: [],
            website: [],
        }, () => {
            this.fetchData()
        })
    }
    handleApplyFilter = () => {
        this.setState({
            open: false,
        }, () => {
            this.fetchData()
        })
    }
    strEllipsis = (str) => {
        if (typeof (str) === "string") {
            if (str.length > 18) {
                return str.substring(0, 18) + "..."
            } else {
                return str
            }
        } else {
            return str
        }
    }
    render() {
        const { classes } = this.props
        const {
            open,
            data,
            rowsPerPage,
            page,
            dense,
            order,
            orderBy,
            department,
            assetType,
            salesType,
            region,
            website,
            departmentSuggestions,
            assetSuggestions,
            salesSuggestions,
            regionSuggestions,
            websiteSuggestions,
            showFilter
        } = this.state
        if (isEmpty(data)) {
            return null
        }
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.results.length - page * rowsPerPage);
        return (
            <React.Fragment>
                <div className={classes.gridContainer}>
                    <Dialog open={open} onClose={this.handleClose} aria-labelledby="form-dialog-title">

                        <DialogTitle id="form-dialog-title">Filter</DialogTitle>

                        <DialogContent>
                            <DialogContentText>
                                You can use the select value filter widget to dynamically filter report data.
                            </DialogContentText>
                            {console.log('departmentSuggestions', departmentSuggestions)}
                            {departmentSuggestions.length ?
                            (<MultiSelection
                                label="Select Department"
                                type="department"
                                suggestions={departmentSuggestions}
                                value={department}
                                setValue={this.handleSelection}
                            />): null}
                            {assetSuggestions.length ?
                            (<MultiSelection
                                label="Select Asset Type"
                                type="assetType"
                                suggestions={assetSuggestions}
                                value={assetType}
                                setValue={this.handleSelection}
                            />) : null }
                            {salesSuggestions.length ?
                            (<MultiSelection
                                label="Select Sales Type"
                                type="salesType"
                                suggestions={salesSuggestions}
                                value={salesType}
                                setValue={this.handleSelection}
                            />): null}
                            {regionSuggestions.length ?
                            (<MultiSelection
                                label="Select Region"
                                type="region"
                                value={region}
                                suggestions={regionSuggestions}
                                setValue={this.handleSelection}
                            />) : null}
                            {websiteSuggestions.length ?
                            (<MultiSelection
                                label="Select website"
                                type="website"
                                value={website}
                                suggestions={websiteSuggestions}
                                setValue={this.handleSelection}
                            />) : null }
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleApplyFilter} color="primary">
                                Apply
                            </Button>
                            <Button onClick={this.handleClear} color="primary">
                                Clear
                            </Button>
                            <Button onClick={this.handleClose} color="primary">
                                Cancel
                    </Button>
                        </DialogActions>
                    </Dialog>
                </div>
                <div className={classes.root}>
                    <Paper className={classes.paper}>
                        {showFilter && <EnhancedTableToolbar handleClick={this.handleClickOpen} />}
                        <div className={classes.tableWrapper}>
                            <Table
                                fixedHeader={false}
                                style={{ width: "auto", tableLayout: "auto" }}
                            >
                                <EnhancedTableHead
                                    order={order}
                                    orderBy={orderBy}
                                    // onSelectAllClick={handleSelectAllClick}
                                    onRequestSort={this.handleRequestSort}
                                    rowCount={data.results.length}
                                />
                                <TableBody>
                                    {stableSort(data.results, getSorting(order, orderBy))
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map(row => {
                                            return (
                                                <StyledTableRow
                                                    key={row.id}
                                                    hover
                                                    // onClick={event => handleClick(event, row.id)}
                                                    // role="checkbox"
                                                    tabIndex={-1}
                                                >
                                                    {headRows.map((rowKey, index) => {
                                                        const val = row[rowKey.id]
                                                        return (
                                                            <StyledTableCell
                                                                key={'row' + rowKey.id}
                                                                align="justify"
                                                                component="th"
                                                                scope="row"
                                                                padding={index === 0 ? "checkbox" : "default"}
                                                            >

                                                                {rowKey.id === "url" ? (
                                                                    <a href={val}>{val}</a>
                                                                ) : (rowKey.id === "scraping_date" || rowKey.id === "last_publication_date")
                                                                        ? convertDate(val)
                                                                        : stringEllipsis(val)
                                                                }

                                                            </StyledTableCell>
                                                        )
                                                    })}

                                                </StyledTableRow>
                                            );
                                        })}
                                    {emptyRows > 0 && (
                                        <TableRow style={{ height: 49 * emptyRows }}>
                                            <StyledTableCell colSpan={6} />
                                        </TableRow>
                                    )}
                                </TableBody>
                            </Table>
                        </div>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25]}
                            component="div"
                            count={data.results.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            backIconButtonProps={{
                                'aria-label': 'Previous Page',
                            }}
                            nextIconButtonProps={{
                                'aria-label': 'Next Page',
                            }}
                            onChangePage={this.handleChangePage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                        />
                    </Paper>
                    <FormControlLabel
                        control={<Switch checked={dense} onChange={this.handleChangeDense} />}
                        label="Dense padding"
                    />
                </div>
            </React.Fragment >
        )
    };
}

export default withStyles(EnhancedTableStyles)(EnhancedTable);
