import { withStyles } from '@material-ui/styles';
import TableCell from '@material-ui/core/TableCell';


const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        padding: 'default'
    },
    body: {
        fontSize: 14,
        // align: 'justify',
        padding: 'default'
    },
}))(TableCell);

export default StyledTableCell