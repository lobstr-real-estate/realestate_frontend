import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
    loader: {
        // height: '10em',
        // position: 'relative'
    },
    process: {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto'
    }
}));

function Loader() {
    const classes = useStyles();

    return (
        <div className={classes.loader}>
            <CircularProgress className={classes.process} />
        </div>
    );
}

export default Loader;
