import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Login from './components/Login'
import Home from './components/Home'
import Loader from './common/Loader'
import './App.css';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#3f51b5',
    },
    secondary: {
      light: '#0066ff',
      main: '#0044ff',
      contrastText: '#ffcc00',
    },
    // error: will use the default color
  },
  typography: {
    fontSize: 14,
  },
  root: {
    spacing: factor => `${0.25 * factor}rem`, // (Bootstrap strategy)
  },
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { authSync: false, isAuthenticated: false };
    this.syncAuth = this.syncAuth.bind(this);
  }
  componentDidMount() {
    // check user status
    this.syncAuth()
  }
  syncAuth() {
    const auth = localStorage.getItem("auth");
    if (auth) {
      this.setState({ authSync: true, isAuthenticated: true })
    } else {
      this.setState({ authSync: true, isAuthenticated: false })
    }
  }
  render() {
    if (!this.state.authSync) {
      return (
        <MuiThemeProvider theme={theme}>
          <Loader />
        </MuiThemeProvider>
      )
    }
    return (
      <MuiThemeProvider theme={theme}>
        {this.state.isAuthenticated ? <Home syncAuth={this.syncAuth} /> : <Login syncAuth={this.syncAuth} />}
      </MuiThemeProvider>
    )
  }
}

export default App;
